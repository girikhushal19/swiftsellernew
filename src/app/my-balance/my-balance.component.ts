import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { WithdrawComponent } from '../withdraw/withdraw.component';

@Component({
  selector: 'app-my-balance',
  templateUrl: './my-balance.component.html',
  styleUrls: ['./my-balance.component.css']
})
export class MyBalanceComponent implements OnInit {
  baseurl:any;main_id:any;balancedata:any;myOptions:any
  constructor(private seller:SellerserviceService, private http:HttpClient,
    private router:Router, private activateroute:ActivatedRoute,public dialog: MatDialog) { 
      this.baseurl = seller.baseapiurl2
      this.main_id = localStorage.getItem('main_sellerid')

      this.myOptions = {
        id:this.main_id,
        start_date:"",
        end_date:""
       
    }

    }

  ngOnInit(): void {
    this.getBalance()
  }
  getBalance(){
    this.http.post(this.baseurl +"api/seller/sellerbalance", this.myOptions).subscribe(res=>{
      this.balancedata = res
      console.log(this.balancedata)
      
    })
  }

 
}
