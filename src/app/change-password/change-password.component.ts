import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  registerform !: FormGroup
  main_id: any
  baseurl: any
  confirm: any
  profile: any
  getstatus: any; main_email: any
  constructor(private seller: SellerserviceService, private http: HttpClient,
    private router: Router) {
    this.main_id = localStorage.getItem('main_sellerid')
    this.main_email = localStorage.getItem('selleremail')
    this.baseurl = seller.baseapiurl2

    this.registerform = new FormGroup({
      password: new FormControl(''),
      newpassword: new FormControl(''),
      confirmpassword: new FormControl('')

    })
  }

  ngOnInit(): void {

  }
  Submitprofile() {
    this.registerform.value.id = this.main_id
    this.registerform.value.email = this.main_email
    console.log(this.registerform.value)

     if (this.registerform.value.newpassword == this.registerform.value.confirmpassword) {
      this.http.post(this.baseurl + "api/seller/createseller", this.registerform.value).subscribe(res => {
        console.log(res)
        this.getstatus = res
        if (this.getstatus.status == true) {
          alert(this.getstatus.message)
          window.location.reload()

        }else{
          alert("votre ancien mot de passe est incorrect")
        }
      })
    }else{
      alert("le mot de passe ne correspond pas")
    }

  }

}
