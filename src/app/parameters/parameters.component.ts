import { Component, OnInit } from '@angular/core';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-parameters',
  templateUrl: './parameters.component.html',
  styleUrls: ['./parameters.component.css']
})
export class ParametersComponent implements OnInit {

  baseurl: any; myoptions: any; main_id: any;settingForm!:FormGroup
  mysetting:any
  constructor(private seller: SellerserviceService,
    private http: HttpClient) {
    this.baseurl = seller.baseapiurl2
    this.main_id = localStorage.getItem('main_sellerid')
    this.mysetting = []
    this.settingForm = new FormGroup({
      user_type:new FormControl('seller'),
      messagefromoblack: new FormControl(),
      emailfromoblack: new FormControl(),
      emailfromuser: new FormControl(),
      messagefromuser: new FormControl()
    })
   
  }

  ngOnInit(): void {
    this.http.get( this.baseurl+"api/common/getnotificationsettings/"+ this.main_id).subscribe(res=>{
      this.mysetting = res
     // console.log(this.mysetting)
      
    })

  }
  setCheck1(eve:any){
    
    this.settingForm.value.emailfromoblack = eve.target.checked
    console.log(this.settingForm.value)
    this.http.post( this.baseurl+"api/common/updatenotificationpermission", this.settingForm.value).subscribe(res=>{
      alert("changements sauvegardés succès")
      window.location.reload()
    })

  }

  send(){
    console.log(this.settingForm.value)
  }

}
