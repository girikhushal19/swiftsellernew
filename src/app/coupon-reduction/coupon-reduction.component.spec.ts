import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CouponReductionComponent } from './coupon-reduction.component';

describe('CouponReductionComponent', () => {
  let component: CouponReductionComponent;
  let fixture: ComponentFixture<CouponReductionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CouponReductionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CouponReductionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
