import { Component, OnInit } from '@angular/core';
import { SellerserviceService } from '../sellerservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-myproducts',
  templateUrl: './myproducts.component.html',
  styleUrls: ['./myproducts.component.css']
})
export class MyproductsComponent implements OnInit {
  baseurl: any
  ordersarray: any
  orders: any;
  myoptions: any
  main_id: any
  searchText: any; APIres: any
  categoryoptions:any
  catagory:any;condition:any; title:any
  getallcatagoriesflat:any; getallcatagoriesflatRes:any; allCat:any
  
  constructor(private seller: SellerserviceService,
    private http: HttpClient, private activeRoute: ActivatedRoute, private router: Router) {
    this.baseurl = seller.baseapiurl2
    this.ordersarray = []
    this.orders = []
    this.main_id = localStorage.getItem('main_sellerid')
    this.getallcatagoriesflat = this.baseurl + "api/product/getallcatagoriesflat"

    this.myoptions = {
      "seller_id": this.main_id,
      "violation": "",
      "instock": "",
      "removed": "",
      "is_stock":""
    }
    this.categoryoptions=[

    ]
  }

  ngOnInit(): void {

    this.http.post(this.baseurl + "api/product/getallproductsforseller", this.myoptions).subscribe(res => {
      this.ordersarray = res
      this.orders = this.ordersarray.data
      console.log(res, "test")
    })

    this.http.get(this.getallcatagoriesflat).subscribe(res=>{
      this.getallcatagoriesflatRes = res
      if(this.getallcatagoriesflatRes.status){
        this.allCat= this.getallcatagoriesflatRes.data
      }
    })


  }
  deleteProduct(id: any) {
    var DywDelete = confirm("Voulez-vous supprimer ce produit ?")
    if (DywDelete == true) {
      this.http.get(this.baseurl + "api/product/deleteproduct/" + id).subscribe(res => {
        this.APIres = res
        if (this.APIres.status == true) {
          //alert("Product deleted Success")
          window.location.reload()
        }
      })
    }
  }
  
  stockcheck(eve:any){
   
    this.myoptions.is_stock = eve.target.value

    this.http.post(this.baseurl + "api/product/getallproductsforseller", this.myoptions).subscribe(res => {
      this.ordersarray = res
      this.orders = this.ordersarray.data
      console.log(res, "test")
    })
  }
}
