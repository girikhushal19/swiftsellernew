import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, AbstractControl, FormControl, Validators } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CategoriesComponent } from '../categories/categories.component';

@Component({
  selector: 'app-edit-branch',
  templateUrl: './edit-branch.component.html',
  styleUrls: ['./edit-branch.component.css']
})
export class EditBranchComponent implements OnInit {

  edit_id:string="";
  registerform!: FormGroup
  baseurl:any;main_id:any;getstatus:any;main_email:any
  endAddress:any; endLatitude:any; endLongitude:any; mypickaddress:any
  formData:any; record:any;
  myFiles:string [] = []; apiResponse:any;
  constructor(private seller:SellerserviceService, private http:HttpClient,private route: ActivatedRoute)
  {
    this.edit_id = this.route.snapshot.queryParams["id"];

    this.baseurl = seller.baseapiurl2
    this.main_id = localStorage.getItem('main_sellerid')
    this.main_email = localStorage.getItem('selleremail');
    
    
    //this.record.

    this.registerform = new FormGroup({       
      shopname:new FormControl('', Validators.required),
      shopdesc:new FormControl('', Validators.required),
      address: new FormControl('', Validators.required)
        
     })

     console.log("this.edit_id " , this.edit_id);
     this.http.get(this.baseurl+"api/seller/getSingleBranch/"+this.edit_id).subscribe(res=>{
      this.apiResponse = res;
        if(this.apiResponse.status ==true)
        {
          this.record = this.apiResponse.record;
          this.endAddress = this.record.address;
          if(this.record.location)
            {
              if(this.record.location.coordinates.length > 0)
              {
                this.endLatitude = this.record.location.coordinates[0];
                this.endLongitude = this.record.location.coordinates[1];
              }
            }
            console.log("this.endLatitude ", this.endLatitude);
            console.log("this.endLongitude ", this.endLongitude);
        }
     })
  }

  ngOnInit(): void {
    this.myFiles = [];
  }
  handleAddressChange2(address2: any) {

    this.endAddress = address2.formatted_address
    console.log( this.endAddress )
    this.endLatitude = address2.geometry.location.lat()
    this.endLongitude = address2.geometry.location.lng()
    console.log( this.endLatitude );
    console.log( this.endLongitude );
    //this.mypickaddress.address = this.endAddress
    //this.mypickaddress.latlong = [this.endLatitude, this.endLongitude]
  }

  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }


  sendBank(){
    this.registerform.value.seller_id = this.main_id
    this.registerform.value.email = this.main_email
    //this.registerform.value.bank_address = this.mypickaddress
    this.registerform.value.address = this.endAddress;
    this.registerform.value.shopLatitude = this.endLatitude;
    this.registerform.value.shopLongitude = this.endLongitude;
    
    this.formData = new FormData();
    for (var i = 0; i < this.myFiles.length; i++)
    { 
      this.formData.append("shopphoto", this.myFiles[i]);
    }
    
    this.formData.append('id', this.edit_id );
    this.formData.append('seller_id', this.main_id );
    this.formData.append('email', this.main_email );
    this.formData.append('address', this.endAddress);
    this.formData.append('shopLatitude', this.endLatitude);
    this.formData.append('shopLongitude', this.endLongitude);
    this.formData.append('shopdesc', this.registerform.value.shopdesc);
    this.formData.append('shopname', this.registerform.value.shopname);
    
    if(this.registerform.valid){
      this.http.post(this.baseurl+"api/seller/updateBranch" , this.formData ).subscribe(res=>{
        this.getstatus =res
        if(this.getstatus.status == true){
          // alert(this.getstatus.message)
          window.location.reload()
        }
      })
    }else{
      for (const control of Object.keys(this.registerform.controls)) {
        this.registerform.controls[control].markAsTouched();
      }
      return;
    }
    
  }

}
