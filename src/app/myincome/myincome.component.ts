import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-myincome',
  templateUrl: './myincome.component.html',
  styleUrls: ['./myincome.component.css']
})
export class MyincomeComponent implements OnInit {
  baseurl:any;main_id:any;incomedata:any;searchText:any
  apires:any
  constructor(private seller:SellerserviceService, private http:HttpClient,
    private router:Router, private activateroute:ActivatedRoute) {
      this.baseurl = seller.baseapiurl2
      this.main_id = localStorage.getItem('main_sellerid')

     }

  ngOnInit(): void {
    this.getInocme()
  }
  getInocme(){
    this.http.get(this.baseurl +"api/seller/sellerincome/"+this.main_id).subscribe(res=>{
      this.apires = res
      if(this.apires.status){
        this.incomedata = this.apires
      }
      

    })
  }

}
