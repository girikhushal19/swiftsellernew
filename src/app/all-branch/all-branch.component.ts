import { Component, OnInit } from '@angular/core';
import { SellerserviceService } from '../sellerservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-all-branch',
  templateUrl: './all-branch.component.html',
  styleUrls: ['./all-branch.component.css']
})
export class AllBranchComponent implements OnInit {
  baseurl: any
  ordersarray: any
  orders: any;
  myoptions: any
  main_id: any
  searchText: any; APIres: any
  categoryoptions:any
  catagory:any;condition:any; title:any; record:any;
  getallcatagoriesflat:any; getallcatagoriesflatRes:any; allCat:any
  
  constructor(private seller: SellerserviceService,
    private http: HttpClient, private activeRoute: ActivatedRoute, private router: Router)
    {
      this.baseurl = seller.baseapiurl2
      this.ordersarray = []
      this.orders = []
      this.main_id = localStorage.getItem('main_sellerid')
      
    }

  ngOnInit(): void {
    this.http.get(this.baseurl+"api/seller/getAllBranch/"+this.main_id).subscribe(res => {
      this.ordersarray = res;
      
      if(this.ordersarray.status == true)
      {
        this.record = this.ordersarray.record;
      }
      
      //this.orders = this.ordersarray.data
      console.log("order ", res);
    })
  }

}
