import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConatctObalckComponent } from './conatct-obalck.component';

describe('ConatctObalckComponent', () => {
  let component: ConatctObalckComponent;
  let fixture: ComponentFixture<ConatctObalckComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConatctObalckComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConatctObalckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
