import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { Chart, registerables } from 'chart.js';
Chart.register(...registerables)

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
  mainid:any
  baseurl:any
  all_data:any;token:any;hostUrl:any
  constructor(private seller:SellerserviceService, private http:HttpClient,
    private router:Router, private activateroute:ActivatedRoute) { 
    this.mainid = localStorage.getItem('main_sellerid') ,
    this.token = localStorage.getItem('token')
    this.baseurl = seller.baseapiurl
    this.hostUrl = seller.hosturl
  }

  ngOnInit(): void {
    console.log(this.mainid , this.hostUrl,this.token )
    if(this.mainid == "" || this.token == ""){
      window.location.href=this.hostUrl+"login"
    }else{
        
    this.http.get(this.baseurl+"renderhomepageSeller/"+this.mainid).subscribe(res=>{
      
      this.all_data = res

      console.log(this.all_data)
      if(this.all_data.data.totoalnumberoforders > 0){
        new Chart('piechart', {
          type: 'doughnut',
          data: {
            labels: [
              'Total',
              'Terminé',
              'Traitement',
              'Annulés',
              'Remboursé',
            ],
            datasets: [{
             
              data:this.all_data.data.orderspie,
    
              backgroundColor: [
                '#2171ae',
                '#28a745',
                '#ffc107',
                'rgb(255, 69, 96)',
                '#17a2b8'
              ],
              hoverOffset: 4
            }]
          },
          options: {
           
            plugins: {
              legend: {
                position: 'left'
              }
    
            }
    
          }
        });
      
      }

      if(this.all_data.data.totalearnings > 0){
        new Chart('bar', {
          type: 'bar',
          data: {
            labels: this.all_data.data.numberofdaysarray,
            datasets: [{
              label: 'Ventes',
              data:this.all_data.data.totalearningsarray,
    
              backgroundColor: [
                '#3d3d3d',
                
              ],
            }]
          },
          options: {
            scales: {
              x: {
                grid: {
                  display: false,
                },
              },
              y: {
                grid: {
                  display: false,
                },
              },
            },
          },
        });
      }
      
  
     
    })
    }
   
  
    
  
    
   
  }

}
