import { Component, OnInit } from '@angular/core';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-my-subscription-pack',
  templateUrl: './my-subscription-pack.component.html',
  styleUrls: ['./my-subscription-pack.component.css']
})
export class MySubscriptionPackComponent implements OnInit {
  baseurl:any; main_id:any
  getSubscriptionProviderId:any; getSubscriptionProviderIdRes:any; subPack:any
  getSubscriptionUrl:any; getSubscriptiondata:any

  mainSubscriptionURL:any;createSubscriptionoptions:any; mainSubAPIres:any
  myemail:any
  constructor(private seller: SellerserviceService, private http: HttpClient,) { 
    this.baseurl = seller.baseapiurl2
    this.main_id = localStorage.getItem('main_sellerid')
    this.myemail = localStorage.getItem('selleremail')
    this.getSubscriptionProviderId = this.baseurl + "api/subs/getSubscriptionProviderId"
    this.getSubscriptionUrl = this.baseurl + 'api/subs/getAllSubscriptions'
    this.mainSubscriptionURL = this.baseurl + 'api/subs/createSubscription'

    this.createSubscriptionoptions = {
      subscriber_name:this.myemail,
      subscriber_email: this.myemail,
      provider_id: this.main_id
    }
  }

  ngOnInit(): void {
    const params={
      "provider_id": this.main_id
    }

    this.http.post( this.getSubscriptionProviderId, params).subscribe(res=>{
      this.getSubscriptionProviderIdRes = res
      if(this.getSubscriptionProviderIdRes.status){
        this.subPack = this.getSubscriptionProviderIdRes.data
        console.log(this.subPack)
      }
    })
    
    this.http.get(this.getSubscriptionUrl).subscribe(res => {
      this.getSubscriptiondata = res
    })
    

  }
  
sendSubscription(subpac:any) {

  this.createSubscriptionoptions.name_of_package = subpac.package
  this.createSubscriptionoptions.duration = subpac.duration
  this.createSubscriptionoptions.package_id = subpac._id

  

  this.http.post(this.mainSubscriptionURL, this.createSubscriptionoptions).subscribe(res => {
    this.mainSubAPIres = res
    if (this.mainSubAPIres.status == true) {
      console.log(this.mainSubAPIres.url);
      window.location.href = this.mainSubAPIres.url
    }
  })
  }
}
