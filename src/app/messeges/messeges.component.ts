import { Component, ElementRef, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { get } from 'jquery';
import { UserchatService } from "../userchat.service";
@Component({
  selector: 'app-messeges',
  templateUrl: './messeges.component.html',
  styleUrls: ['./messeges.component.css']
})
export class MessegesComponent implements OnInit {
  main_id: any
  baseurl: any
  chatsURL: any;
  apiResponse: any;
  allChats: any;
  chatRoomURL: any
  sellerChat: any
  sendID: any
  sendOptions: any
  mymessege: any
  sendAPI: any
  contentRef!: ElementRef;
  contentHeight: any
  delete_chat_seller_by_room_id:any; delete_chat_seller_by_room_idRes:any
  constructor(private seller: SellerserviceService, private http: HttpClient,
    private router: Router, private activateroute: ActivatedRoute, private userChatService: UserchatService) {
    this.main_id = localStorage.getItem('main_sellerid')
    this.baseurl = seller.baseapiurl2
    this.chatsURL = this.baseurl + "api/chat/get_all_seller_chat/"
    this.chatRoomURL = this.baseurl + "api/chat/get_all_message_room_seller/"
    this.sendAPI = this.baseurl + "api/chat/new_initiate_chat"
    this.delete_chat_seller_by_room_id = this.baseurl +"api/chat/delete_chat_seller_by_room_id/"
    this.sellerChat = []
    this.sendOptions = {
      message_by: 2
    }
    this.http.get(this.chatsURL + this.main_id).subscribe(res => {
      this.allChats = res
    })
  }

  ngOnInit(): void {
    this.userChatService.getNewMessage().subscribe((response: any) => {
      console.log("hereeeeeeeeeeee 45");
      this.apiResponse = response;
      this.sellerChat = this.apiResponse.chat;
      // console.log(this.sellerChat, "j")
      // this.contentHeight = this.contentRef.nativeElement.scrollHeight;
      // if (this.sellerChat) {
      //   const assenData = this.sellerChat.chat.sort((a: { created_at: string | number | Date; }, b: { created_at: string | number | Date; }) => {
      //     return <any>new Date(a.created_at) - <any>new Date(b.created_at);
      //   });
      //   // console.log(assenData, 'hh')
      // }
    })


  }
  gotoChat(roomid: any) {
    console.log(roomid._id)
    this.sendID = roomid
    // setInterval(() => {
    this.http.get(this.chatRoomURL + roomid._id).subscribe(res => {
      this.apiResponse = res
      this.sellerChat = this.apiResponse.data;
      console.log(this.sellerChat, "j")
      // if (this.sellerChat) {
      //   const assenData = this.sellerChat.data.sort((a: { created_at: string | number | Date; }, b: { created_at: string | number | Date; }) => {
      //     return <any>new Date(a.created_at) - <any>new Date(b.created_at);
      //   });
      //   // console.log(assenData, 'hh')
      // }
    })
    // }, 3000);



  }

  sendMessege() {
    // console.log(this.sendID)

    //{product_id,seller_id,user_id,message,message_by} 
    this.sendOptions.seller_id = this.sendID.seller_id
    this.sendOptions.user_id = this.sendID.user_id
    this.sendOptions.message = this.mymessege.value
    this.sendOptions.product_id = this.sendID.product_id
    this.sendOptions.message_by = 1;
    //console.log(this.sendOptions, "test")
    // this.http.post(this.sendAPI, this.sendOptions).subscribe(res => {
    //   console.log(res)
    //   this.mymessege.value = ''
    //   //this.contentHeight = this.contentRef.nativeElement.scrollHeight;
    // })
    this.userChatService.sendMessage(this.sendOptions);
    this.mymessege.value = ''
    ///this.contentHeight = this.contentRef.nativeElement.scrollHeight;
    // "product_id":"63f5bd6c5bf0bda586960960",
    // "seller_id":"63f5c1ec5bf0bda586960971",
    // "user_id":"642d5d64f5a88587ffae0ab5",
    // "message":"Test 2",
    // "message_by":1
  }

  deletemyChat(mychat:any){
    
    const deleteId= mychat._id
    const conf = confirm("Voulez-vous supprimer ce chat ?")
    if(conf){
      this.http.get(this.delete_chat_seller_by_room_id+deleteId).subscribe(res=>{
        this.delete_chat_seller_by_room_idRes = res
        if(this.delete_chat_seller_by_room_idRes.status){
          window.location.reload()
        }
      })
    }
    
  }


}


