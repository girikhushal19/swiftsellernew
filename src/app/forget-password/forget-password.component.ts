import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  loginform!: FormGroup;baseurl:any;forgetpassAPI:any;Apiresposnse:any
  constructor(private seller: SellerserviceService, private http: HttpClient,) {
    this.baseurl = seller.baseapiurl2
    this.forgetpassAPI =  this.baseurl+"api/seller/sellerForgotPassword"
    this.loginform = new FormGroup({
      email: new FormControl('', [ Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
   
    })
   }

  ngOnInit(): void {
  }
  reset(){
    if(this.loginform.valid ){
      console.log(this.loginform.value)

      this.http.post(this.forgetpassAPI,this.loginform.value ).subscribe(res=>{
        this.Apiresposnse =  res
        this.loginform.reset()
        console.log(this.Apiresposnse)
      })



    }else{
      for (const control of Object.keys(this.loginform.controls)) {
        this.loginform.controls[control].markAsTouched();
      }
      return;
    }
    
  }

}
