import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadStripeDocumentComponent } from './upload-stripe-document.component';

describe('UploadStripeDocumentComponent', () => {
  let component: UploadStripeDocumentComponent;
  let fixture: ComponentFixture<UploadStripeDocumentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadStripeDocumentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UploadStripeDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
