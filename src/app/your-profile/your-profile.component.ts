import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { EditProfileComponent } from '../edit-profile/edit-profile.component';
import { ChangePasswordComponent } from '../change-password/change-password.component';
@Component({
  selector: 'app-your-profile',
  templateUrl: './your-profile.component.html',
  styleUrls: ['./your-profile.component.css']
})
export class YourProfileComponent implements OnInit {
  mainid: any
  baseurl: any
  profile: any
  deletestatus: any
  hosturl: any
  constructor(private seller: SellerserviceService, private http: HttpClient,
    private router: Router, private activateroute: ActivatedRoute, public dialog: MatDialog) {
    this.mainid = localStorage.getItem('main_sellerid')
    this.baseurl = seller.baseapiurl2
    this.hosturl = seller.hosturl

  }

  ngOnInit(): void {
    this.http.get(this.baseurl + "api/seller/getuserprofile/" + this.mainid).subscribe(res => {

      this.profile = res
      console.log(this.profile)

    })
  }
  editProfile(id: any) {
    console.log(id)
  }
  deleteAccount() {
    console.log(this.mainid)
    var myconfirm = confirm("Êtes-vous sûr de vouloir supprimer votre compte ?")
    if(myconfirm){
      this.http.get(this.baseurl + "api/seller/deletevendor/" + this.mainid).subscribe(res => {
        this.deletestatus = res
        if (this.deletestatus.status == true) {
          localStorage.clear()
          window.location.href = this.hosturl + "/login"
        }
  
      })
    }
    
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(EditProfileComponent, {
      height: '750px',
      width: '700px',
      position: {
        top: '7vh',
        left: '30vw'
      },
    });
  }
  openDialog2(): void {
    const dialogRef = this.dialog.open(ChangePasswordComponent, {
      height: '600px',
      width: '700px',
      position: {
        top: '11vh',
        left: '30vw'
      },
    });
  }
  

}
