import { Component, OnInit } from '@angular/core';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  baseurl: any; myoptions: any; main_id: any;mynotificatuons:any
  getSellerNotification:any; getSellerNotificationRes:any

  getDeleteNotificationById:any; getDeleteNotificationByIdRes:any
  getDeleteNotificationByUserId:any; getDeleteNotificationByUserIdRes:any
  constructor(private seller: SellerserviceService,
    private http: HttpClient) {
    this.baseurl = seller.baseapiurl2
    this.mynotificatuons = []
    this.main_id = localStorage.getItem('main_sellerid')
    this.getSellerNotification = this.baseurl+"api/common/getSellerNotification/"+this.main_id
    this.getDeleteNotificationById = this.baseurl+"api/common/getDeleteNotificationById/"
    this.getDeleteNotificationByUserId = this.baseurl+"api/common/getDeleteNotificationByUserId/"+this.main_id
    this.myoptions = {
      client_id: this.main_id,
      type:"read"
    }
  }

  ngOnInit(): void {
    this.http.get( this.getSellerNotification).subscribe(res=>{
      this.getSellerNotificationRes = res
      if(this.getSellerNotificationRes.status){
        this.mynotificatuons = this.getSellerNotificationRes.data
        
      }
    })

  }

  delete(singleID:any){
    this.http.get(this.getDeleteNotificationById + singleID).subscribe(res=>{
      this.getDeleteNotificationByIdRes = res
      if( this.getDeleteNotificationByIdRes.status){
        window.location.reload()
      }
    })
  }

  deleteAll(){
    const conf = confirm("Voulez-vous supprimer toutes les notifications ?")
    if(conf){
      this.http.get(this.getDeleteNotificationByUserId ).subscribe(res=>{
        this.getDeleteNotificationByUserIdRes = res
        if( this.getDeleteNotificationByUserIdRes.status){
          window.location.reload()
        }
      })
    }


  }

}
