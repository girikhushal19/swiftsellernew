import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

@Component({
  selector: 'app-myorders',
  templateUrl: './myorders.component.html',
  styleUrls: ['./myorders.component.css']
})
export class MyordersComponent implements OnInit {
  main_id: any
  baseurl: any
  ordersarray: any
  orders: any;
  myoptions: any
  searchText: any
  showerror: any
  start_date: any; end_date: any
  getCancelOrdersBySellerId:any; getCancelOrdersBySellerIdRes:any; cancelOrders:any
  getProcessOrdersBySellerId:any; getProcessOrdersBySellerIdRes:any; processOrdes:any
  getCompletedOrdersBySellerId:any; getCompletedOrdersBySellerIdRes:any; completeOrdes:any
  
  constructor(private seller: SellerserviceService, private http: HttpClient,
    private router: Router, private activateroute: ActivatedRoute,private adapter: DateAdapter<any>) {
    this.main_id = localStorage.getItem('main_sellerid')
    this.baseurl = seller.baseapiurl2
    this.getCancelOrdersBySellerId = this.baseurl + "api/order/getCancelOrdersBySellerId"
    this.getProcessOrdersBySellerId = this.baseurl + "api/order/getProcessOrdersBySellerId"
    this.getCompletedOrdersBySellerId = this.baseurl + "api/order/getCompletedOrdersBySellerId"

    // this.ordersarray = []
    // this.orders = []

    this.myoptions = {
      start_date: "",
      end_date: "",
      order_status: "",
      id: this.main_id
    }


  }

  ngOnInit(): void {

    this.http.post(this.baseurl + "api/order/getallordersbysellerid", this.myoptions).subscribe(res => {
      console.log(res)
      this.ordersarray = res
      this.orders = this.ordersarray.data
     

    })

    const params = {
      "id":this.main_id
    }

    this.http.post(this.getCancelOrdersBySellerId, params).subscribe(res => {
      this.getCancelOrdersBySellerIdRes = res
      if(this.getCancelOrdersBySellerIdRes.status){
        this.cancelOrders  = this.getCancelOrdersBySellerIdRes.data
      }
     

    })

    this.http.post(this.getProcessOrdersBySellerId, params).subscribe(res => {
      this.getProcessOrdersBySellerIdRes = res
      if(this.getProcessOrdersBySellerIdRes.status){
        this.processOrdes  = this.getProcessOrdersBySellerIdRes.data
      }
     

    })

    this.http.post(this.getCompletedOrdersBySellerId, params).subscribe(res => {
      this.getCompletedOrdersBySellerIdRes = res
      if(this.getCompletedOrdersBySellerIdRes.status){
        this.completeOrdes  = this.getCompletedOrdersBySellerIdRes.data
      }
     

    })



  }
  filterDate() {

    this.myoptions.start_date = this.start_date.value
    this.myoptions.end_date = this.end_date.value
    console.log(this.myoptions)
    this.http.post(this.baseurl + "api/order/getallordersbysellerid", this.myoptions).subscribe(res => {
      console.log(res)
      this.ordersarray = res
      this.orders = this.ordersarray.data
      // console.log(this.orders[0].sellerproducts[0].products.quantity)
      // console.log(this.orders)

    })

  }



}
