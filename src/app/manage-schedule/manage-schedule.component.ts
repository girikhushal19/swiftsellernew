import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, AbstractControl, FormControl, Validators } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';  

@Component({
  selector: 'app-manage-schedule',
  templateUrl: './manage-schedule.component.html',
  styleUrls: ['./manage-schedule.component.css']
})
export class ManageScheduleComponent implements OnInit {

  userForm!: FormGroup; productForm!: FormGroup;
  form!: FormGroup; main_id:any="";
  deliveryType:number=1; apiResponse:any;
  mondayValue:Boolean = true; tuesdayValue:Boolean = true; wednesdayValue:Boolean = true;
  thrusdayValue:Boolean = true; fridayValue:Boolean = true; saturdayValue:Boolean = true;
  sundayValue:Boolean = true;
  add_quantity_number:number = 1; add_quantity_number_tuesday:number = 1;  add_quantity_number_wednesday:number = 1; 
  add_quantity_number_thrusday:number = 1; add_quantity_number_friday:number = 1; 
  add_quantity_number_saturday:number = 1; add_quantity_number_sunday:number = 1;

  monday_start_time_0:number = 0; monday_end_time_0:number = 0;
  monday_start_time_1:number = 0; monday_end_time_1:number = 0;
  monday_start_time_2:number = 0; monday_end_time_2:number = 0;
  monday_start_time_3:number = 0; monday_end_time_3:number = 0;

  tuesday_start_time_0:number = 0; tuesday_end_time_0:number = 0;
  tuesday_start_time_1:number = 0; tuesday_end_time_1:number = 0;
  tuesday_start_time_2:number = 0; tuesday_end_time_2:number = 0;
  tuesday_start_time_3:number = 0; tuesday_end_time_3:number = 0;

  wednesday_start_time_0:number = 0; wednesday_end_time_0:number = 0;
  wednesday_start_time_1:number = 0; wednesday_end_time_1:number = 0;
  wednesday_start_time_2:number = 0; wednesday_end_time_2:number = 0;
  wednesday_start_time_3:number = 0; wednesday_end_time_3:number = 0;

  thrusday_start_time_0:number = 0; thrusday_end_time_0:number = 0;
  thrusday_start_time_1:number = 0; thrusday_end_time_1:number = 0;
  thrusday_start_time_2:number = 0; thrusday_end_time_2:number = 0;
  thrusday_start_time_3:number = 0; thrusday_end_time_3:number = 0;

  friday_start_time_0:number = 0; friday_end_time_0:number = 0;
  friday_start_time_1:number = 0; friday_end_time_1:number = 0;
  friday_start_time_2:number = 0; friday_end_time_2:number = 0;
  friday_start_time_3:number = 0; friday_end_time_3:number = 0;

  saturday_start_time_0:number = 0; saturday_end_time_0:number = 0;
  saturday_start_time_1:number = 0; saturday_end_time_1:number = 0;
  saturday_start_time_2:number = 0; saturday_end_time_2:number = 0;
  saturday_start_time_3:number = 0; saturday_end_time_3:number = 0;

  sunday_start_time_0:number = 0; sunday_end_time_0:number = 0;
  sunday_start_time_1:number = 0; sunday_end_time_1:number = 0;
  sunday_start_time_2:number = 0; sunday_end_time_2:number = 0;
  sunday_start_time_3:number = 0; sunday_end_time_3:number = 0;
  base_url: string = '';
  constructor(private fb: FormBuilder, private seller: SellerserviceService,
    private http: HttpClient, private activeRoute: ActivatedRoute, private router: Router,
    private route: ActivatedRoute ) {

      this.base_url = seller.baseapiurl2;
      this.main_id = localStorage.getItem('main_sellerid')
      
      console.log("this.main_id ", this.main_id);

      this.productForm = this.fb.group({
        seller_id: new FormControl('', []),
        deliveryType: new FormControl('', []),
        monday: new FormControl('', []),
        tuesday: new FormControl('', []),
        wednesday: new FormControl('', []),
        thrusday: new FormControl('', []),
        friday: new FormControl('', []),
        saturday: new FormControl('', []),
        sunday: new FormControl('', []),
        quantities: this.fb.array([]),
        quantitiesTuesday: this.fb.array([]) ,
        quantitieswednesday: this.fb.array([]) ,
        quantitiesthrusday: this.fb.array([]) ,
        quantitiesfriday: this.fb.array([]) ,
        quantitiessaturday: this.fb.array([]) ,
        quantitiessunday: this.fb.array([]) ,
         
        //secondMainVariation: new FormControl('', []),
        //product_id: '', 
        //secondMainVariation: '',
        
        
        //quantitiesTuesday: this.fb.array([]) , 
      });
      this.addQuantity();
      this.addQuantityTuesday();
      this.addQuantitywednesday();
      this.addQuantitythrusday();
      this.addQuantityfriday();
      this.addQuantitysaturday();
      this.addQuantitysunday();
    }

  ngOnInit(): void {
  }
  myClickOnDeliveryType(val:any)
  {
    //1 = Delivery , 2 = Pick up , 3 = Try On Program

    this.deliveryType = val;
    console.log("//1 = Delivery , 2 = Pick up , 3 = Try On Program");
    console.log(this.deliveryType);
  }
  
  mondayIncreaseStartTimeFun(val:any)
  {
    //console.log("i",val);
    if(val == 0)
    {
      if(this.monday_start_time_0 < 23)
      {
        this.monday_start_time_0++;
      }
    }
    if(val == 1)
    {
      if(this.monday_start_time_1 < 23)
      {
        this.monday_start_time_1++;
      }
    }
    if(val == 2)
    {
      if(this.monday_start_time_2 < 23)
      {
        this.monday_start_time_2++;
      }
    }
    if(val == 3)
    {
      if(this.monday_start_time_3 < 23)
      {
        this.monday_start_time_3++;
      }
    }
    
  }
  mondayDecreaseStartTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.monday_start_time_0 > 0)
      {
        this.monday_start_time_0--;
      }
    }
    if(val == 1)
    {
      if(this.monday_start_time_1 > 0)
      {
        this.monday_start_time_1--;
      }
    }
    if(val == 2)
    {
      if(this.monday_start_time_2 > 0)
      {
        this.monday_start_time_2--;
      }
    }
    if(val == 3)
    {
      if(this.monday_start_time_3 > 0)
      {
        this.monday_start_time_3--;
      }
    }
  }
  mondayIncreaseEndTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.monday_end_time_0 < 23)
      {
        this.monday_end_time_0++;
      }
    }
    if(val == 1)
    {
      if(this.monday_end_time_1 < 23)
      {
        this.monday_end_time_1++;
      }
    }
    if(val == 2)
    {
      if(this.monday_end_time_2 < 23)
      {
        this.monday_end_time_2++;
      }
    }
    if(val == 3)
    {
      if(this.monday_end_time_3 < 23)
      {
        this.monday_end_time_3++;
      }
    }
  }
  mondayDecreaseEndTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.monday_end_time_0 > 0)
      {
        this.monday_end_time_0--;
      }
    }
    if(val == 1)
    {
      if(this.monday_end_time_1 > 0)
      {
        this.monday_end_time_1--;
      }
    }
    if(val == 2)
    {
      if(this.monday_end_time_2 > 0)
      {
        this.monday_end_time_2--;
      }
    }
    if(val == 3)
    {
      if(this.monday_end_time_3 > 0)
      {
        this.monday_end_time_3--;
      }
    }
    
  }

  tuesdayIncreaseStartTimeFun(val:any)
  {
    //console.log("i",val);
    if(val == 0)
    {
      if(this.tuesday_start_time_0 < 23)
      {
        this.tuesday_start_time_0++;
      }
    }
    if(val == 1)
    {
      if(this.tuesday_start_time_1 < 23)
      {
        this.tuesday_start_time_1++;
      }
    }
    if(val == 2)
    {
      if(this.tuesday_start_time_2 < 23)
      {
        this.tuesday_start_time_2++;
      }
    }
    if(val == 3)
    {
      if(this.tuesday_start_time_3 < 23)
      {
        this.tuesday_start_time_3++;
      }
    }
    
  }
  tuesdayDecreaseStartTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.tuesday_start_time_0 > 0)
      {
        this.tuesday_start_time_0--;
      }
    }
    if(val == 1)
    {
      if(this.tuesday_start_time_1 > 0)
      {
        this.tuesday_start_time_1--;
      }
    }
    if(val == 2)
    {
      if(this.tuesday_start_time_2 > 0)
      {
        this.tuesday_start_time_2--;
      }
    }
    if(val == 3)
    {
      if(this.tuesday_start_time_3 > 0)
      {
        this.tuesday_start_time_3--;
      }
    }
  }
  tuesdayIncreaseEndTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.tuesday_end_time_0 < 23)
      {
        this.tuesday_end_time_0++;
      }
    }
    if(val == 1)
    {
      if(this.tuesday_end_time_1 < 23)
      {
        this.tuesday_end_time_1++;
      }
    }
    if(val == 2)
    {
      if(this.tuesday_end_time_2 < 23)
      {
        this.tuesday_end_time_2++;
      }
    }
    if(val == 3)
    {
      if(this.tuesday_end_time_3 < 23)
      {
        this.tuesday_end_time_3++;
      }
    }
  }
  tuesdayDecreaseEndTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.tuesday_end_time_0 > 0)
      {
        this.tuesday_end_time_0--;
      }
    }
    if(val == 1)
    {
      if(this.tuesday_end_time_1 > 0)
      {
        this.tuesday_end_time_1--;
      }
    }
    if(val == 2)
    {
      if(this.tuesday_end_time_2 > 0)
      {
        this.tuesday_end_time_2--;
      }
    }
    if(val == 3)
    {
      if(this.tuesday_end_time_3 > 0)
      {
        this.tuesday_end_time_3--;
      }
    }
    
  }


  wednesdayIncreaseStartTimeFun(val:any)
  {
    //console.log("i",val);
    if(val == 0)
    {
      if(this.wednesday_start_time_0 < 23)
      {
        this.wednesday_start_time_0++;
      }
    }
    if(val == 1)
    {
      if(this.wednesday_start_time_1 < 23)
      {
        this.wednesday_start_time_1++;
      }
    }
    if(val == 2)
    {
      if(this.wednesday_start_time_2 < 23)
      {
        this.wednesday_start_time_2++;
      }
    }
    if(val == 3)
    {
      if(this.wednesday_start_time_3 < 23)
      {
        this.wednesday_start_time_3++;
      }
    }
    
  }
  wednesdayDecreaseStartTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.wednesday_start_time_0 > 0)
      {
        this.wednesday_start_time_0--;
      }
    }
    if(val == 1)
    {
      if(this.wednesday_start_time_1 > 0)
      {
        this.wednesday_start_time_1--;
      }
    }
    if(val == 2)
    {
      if(this.wednesday_start_time_2 > 0)
      {
        this.wednesday_start_time_2--;
      }
    }
    if(val == 3)
    {
      if(this.wednesday_start_time_3 > 0)
      {
        this.wednesday_start_time_3--;
      }
    }
  }
  wednesdayIncreaseEndTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.wednesday_end_time_0 < 23)
      {
        this.wednesday_end_time_0++;
      }
    }
    if(val == 1)
    {
      if(this.wednesday_end_time_1 < 23)
      {
        this.wednesday_end_time_1++;
      }
    }
    if(val == 2)
    {
      if(this.wednesday_end_time_2 < 23)
      {
        this.wednesday_end_time_2++;
      }
    }
    if(val == 3)
    {
      if(this.wednesday_end_time_3 < 23)
      {
        this.wednesday_end_time_3++;
      }
    }
  }
  wednesdayDecreaseEndTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.wednesday_end_time_0 > 0)
      {
        this.wednesday_end_time_0--;
      }
    }
    if(val == 1)
    {
      if(this.wednesday_end_time_1 > 0)
      {
        this.wednesday_end_time_1--;
      }
    }
    if(val == 2)
    {
      if(this.wednesday_end_time_2 > 0)
      {
        this.wednesday_end_time_2--;
      }
    }
    if(val == 3)
    {
      if(this.wednesday_end_time_3 > 0)
      {
        this.wednesday_end_time_3--;
      }
    }
    
  }

  thrusdayIncreaseStartTimeFun(val:any)
  {
    //console.log("i",val);
    if(val == 0)
    {
      if(this.thrusday_start_time_0 < 23)
      {
        this.thrusday_start_time_0++;
      }
    }
    if(val == 1)
    {
      if(this.thrusday_start_time_1 < 23)
      {
        this.thrusday_start_time_1++;
      }
    }
    if(val == 2)
    {
      if(this.thrusday_start_time_2 < 23)
      {
        this.thrusday_start_time_2++;
      }
    }
    if(val == 3)
    {
      if(this.thrusday_start_time_3 < 23)
      {
        this.thrusday_start_time_3++;
      }
    }
    
  }
  thrusdayDecreaseStartTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.thrusday_start_time_0 > 0)
      {
        this.thrusday_start_time_0--;
      }
    }
    if(val == 1)
    {
      if(this.thrusday_start_time_1 > 0)
      {
        this.thrusday_start_time_1--;
      }
    }
    if(val == 2)
    {
      if(this.thrusday_start_time_2 > 0)
      {
        this.thrusday_start_time_2--;
      }
    }
    if(val == 3)
    {
      if(this.thrusday_start_time_3 > 0)
      {
        this.thrusday_start_time_3--;
      }
    }
  }
  thrusdayIncreaseEndTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.thrusday_end_time_0 < 23)
      {
        this.thrusday_end_time_0++;
      }
    }
    if(val == 1)
    {
      if(this.thrusday_end_time_1 < 23)
      {
        this.thrusday_end_time_1++;
      }
    }
    if(val == 2)
    {
      if(this.thrusday_end_time_2 < 23)
      {
        this.thrusday_end_time_2++;
      }
    }
    if(val == 3)
    {
      if(this.thrusday_end_time_3 < 23)
      {
        this.thrusday_end_time_3++;
      }
    }
  }
  thrusdayDecreaseEndTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.thrusday_end_time_0 > 0)
      {
        this.thrusday_end_time_0--;
      }
    }
    if(val == 1)
    {
      if(this.thrusday_end_time_1 > 0)
      {
        this.thrusday_end_time_1--;
      }
    }
    if(val == 2)
    {
      if(this.thrusday_end_time_2 > 0)
      {
        this.thrusday_end_time_2--;
      }
    }
    if(val == 3)
    {
      if(this.thrusday_end_time_3 > 0)
      {
        this.thrusday_end_time_3--;
      }
    }
    
  }

  fridayIncreaseStartTimeFun(val:any)
  {
    //console.log("i",val);
    if(val == 0)
    {
      if(this.friday_start_time_0 < 23)
      {
        this.friday_start_time_0++;
      }
    }
    if(val == 1)
    {
      if(this.friday_start_time_1 < 23)
      {
        this.friday_start_time_1++;
      }
    }
    if(val == 2)
    {
      if(this.friday_start_time_2 < 23)
      {
        this.friday_start_time_2++;
      }
    }
    if(val == 3)
    {
      if(this.friday_start_time_3 < 23)
      {
        this.friday_start_time_3++;
      }
    }
    
  }
  fridayDecreaseStartTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.friday_start_time_0 > 0)
      {
        this.friday_start_time_0--;
      }
    }
    if(val == 1)
    {
      if(this.friday_start_time_1 > 0)
      {
        this.friday_start_time_1--;
      }
    }
    if(val == 2)
    {
      if(this.friday_start_time_2 > 0)
      {
        this.friday_start_time_2--;
      }
    }
    if(val == 3)
    {
      if(this.friday_start_time_3 > 0)
      {
        this.friday_start_time_3--;
      }
    }
  }
  fridayIncreaseEndTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.friday_end_time_0 < 23)
      {
        this.friday_end_time_0++;
      }
    }
    if(val == 1)
    {
      if(this.friday_end_time_1 < 23)
      {
        this.friday_end_time_1++;
      }
    }
    if(val == 2)
    {
      if(this.friday_end_time_2 < 23)
      {
        this.friday_end_time_2++;
      }
    }
    if(val == 3)
    {
      if(this.friday_end_time_3 < 23)
      {
        this.friday_end_time_3++;
      }
    }
  }
  fridayDecreaseEndTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.friday_end_time_0 > 0)
      {
        this.friday_end_time_0--;
      }
    }
    if(val == 1)
    {
      if(this.friday_end_time_1 > 0)
      {
        this.friday_end_time_1--;
      }
    }
    if(val == 2)
    {
      if(this.friday_end_time_2 > 0)
      {
        this.friday_end_time_2--;
      }
    }
    if(val == 3)
    {
      if(this.friday_end_time_3 > 0)
      {
        this.friday_end_time_3--;
      }
    }
    
  }


  saturdayIncreaseStartTimeFun(val:any)
  {
    //console.log("i",val);
    if(val == 0)
    {
      if(this.saturday_start_time_0 < 23)
      {
        this.saturday_start_time_0++;
      }
    }
    if(val == 1)
    {
      if(this.saturday_start_time_1 < 23)
      {
        this.saturday_start_time_1++;
      }
    }
    if(val == 2)
    {
      if(this.saturday_start_time_2 < 23)
      {
        this.saturday_start_time_2++;
      }
    }
    if(val == 3)
    {
      if(this.saturday_start_time_3 < 23)
      {
        this.saturday_start_time_3++;
      }
    }
    
  }
  saturdayDecreaseStartTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.saturday_start_time_0 > 0)
      {
        this.saturday_start_time_0--;
      }
    }
    if(val == 1)
    {
      if(this.saturday_start_time_1 > 0)
      {
        this.saturday_start_time_1--;
      }
    }
    if(val == 2)
    {
      if(this.saturday_start_time_2 > 0)
      {
        this.saturday_start_time_2--;
      }
    }
    if(val == 3)
    {
      if(this.saturday_start_time_3 > 0)
      {
        this.saturday_start_time_3--;
      }
    }
  }
  saturdayIncreaseEndTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.saturday_end_time_0 < 23)
      {
        this.saturday_end_time_0++;
      }
    }
    if(val == 1)
    {
      if(this.saturday_end_time_1 < 23)
      {
        this.saturday_end_time_1++;
      }
    }
    if(val == 2)
    {
      if(this.saturday_end_time_2 < 23)
      {
        this.saturday_end_time_2++;
      }
    }
    if(val == 3)
    {
      if(this.saturday_end_time_3 < 23)
      {
        this.saturday_end_time_3++;
      }
    }
  }
  saturdayDecreaseEndTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.saturday_end_time_0 > 0)
      {
        this.saturday_end_time_0--;
      }
    }
    if(val == 1)
    {
      if(this.saturday_end_time_1 > 0)
      {
        this.saturday_end_time_1--;
      }
    }
    if(val == 2)
    {
      if(this.saturday_end_time_2 > 0)
      {
        this.saturday_end_time_2--;
      }
    }
    if(val == 3)
    {
      if(this.saturday_end_time_3 > 0)
      {
        this.saturday_end_time_3--;
      }
    }
    
  }


  sundayIncreaseStartTimeFun(val:any)
  {
    //console.log("i",val);
    if(val == 0)
    {
      if(this.sunday_start_time_0 < 23)
      {
        this.sunday_start_time_0++;
      }
    }
    if(val == 1)
    {
      if(this.sunday_start_time_1 < 23)
      {
        this.sunday_start_time_1++;
      }
    }
    if(val == 2)
    {
      if(this.sunday_start_time_2 < 23)
      {
        this.sunday_start_time_2++;
      }
    }
    if(val == 3)
    {
      if(this.sunday_start_time_3 < 23)
      {
        this.sunday_start_time_3++;
      }
    }
    
  }
  sundayDecreaseStartTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.sunday_start_time_0 > 0)
      {
        this.sunday_start_time_0--;
      }
    }
    if(val == 1)
    {
      if(this.sunday_start_time_1 > 0)
      {
        this.sunday_start_time_1--;
      }
    }
    if(val == 2)
    {
      if(this.sunday_start_time_2 > 0)
      {
        this.sunday_start_time_2--;
      }
    }
    if(val == 3)
    {
      if(this.sunday_start_time_3 > 0)
      {
        this.sunday_start_time_3--;
      }
    }
  }
  sundayIncreaseEndTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.sunday_end_time_0 < 23)
      {
        this.sunday_end_time_0++;
      }
    }
    if(val == 1)
    {
      if(this.sunday_end_time_1 < 23)
      {
        this.sunday_end_time_1++;
      }
    }
    if(val == 2)
    {
      if(this.sunday_end_time_2 < 23)
      {
        this.sunday_end_time_2++;
      }
    }
    if(val == 3)
    {
      if(this.sunday_end_time_3 < 23)
      {
        this.sunday_end_time_3++;
      }
    }
  }
  sundayDecreaseEndTimeFun(val:any)
  {
    if(val == 0)
    {
      if(this.sunday_end_time_0 > 0)
      {
        this.sunday_end_time_0--;
      }
    }
    if(val == 1)
    {
      if(this.sunday_end_time_1 > 0)
      {
        this.sunday_end_time_1--;
      }
    }
    if(val == 2)
    {
      if(this.sunday_end_time_2 > 0)
      {
        this.sunday_end_time_2--;
      }
    }
    if(val == 3)
    {
      if(this.sunday_end_time_3 > 0)
      {
        this.sunday_end_time_3--;
      }
    }
    
  }
  quantities() : FormArray {
    return this.productForm.get("quantities") as FormArray
  }
  addQuantity() {
    if(this.add_quantity_number <= 4)
    {
      console.log("this.add_quantity_numbe ", this.add_quantity_number);
  
      this.add_quantity_number++;
      this.quantities().push(this.newQuantity());
    }
  }
  removeQuantity(i:number)
  {
    console.log("i ", i);
    if(i>0)
    {
      this.add_quantity_number--;
      this.quantities().removeAt(i);
    }
    
  }
  newQuantity(): FormGroup {
    return this.fb.group({
      mondayStartTime: '',
      mondayEndTime: ''
    })
  }
  quantitiesTuesday() : FormArray {
    return this.productForm.get("quantitiesTuesday") as FormArray
  }
  addQuantityTuesday() {
    if(this.add_quantity_number_tuesday <= 4)
    {
      console.log("this.add_quantity_number_tuesday ", this.add_quantity_number_tuesday);
  
      this.add_quantity_number_tuesday++;
      this.quantitiesTuesday().push(this.newQuantityTuesday());
    }
  }
  removeQuantityTuesday(i:number)
  {
    console.log("i ", i);
    if(i>0)
    {
      this.add_quantity_number_tuesday--;
      this.quantitiesTuesday().removeAt(i);
    }
    
  }
  newQuantityTuesday(): FormGroup {
    return this.fb.group({
      tuesdayStartTime: '',
      tuesdayEndTime: ''
    })
  }
  

  quantitieswednesday() : FormArray {
    return this.productForm.get("quantitieswednesday") as FormArray
  }
  addQuantitywednesday() {
    if(this.add_quantity_number_wednesday <= 4)
    {
      console.log("this.add_quantity_number_wednesday ", this.add_quantity_number_wednesday);
  
      this.add_quantity_number_wednesday++;
      this.quantitieswednesday().push(this.newQuantitywednesday());
    }
  }
  removeQuantitywednesday(i:number)
  {
    console.log("i ", i);
    if(i>0)
    {
      this.add_quantity_number_wednesday--;
      this.quantitieswednesday().removeAt(i);
    }
    
  }
  newQuantitywednesday(): FormGroup {
    return this.fb.group({
      wednesdayStartTime: '',
      wednesdayEndTime: ''
    })
  }


  quantitiesthrusday() : FormArray {
    return this.productForm.get("quantitiesthrusday") as FormArray
  }
  addQuantitythrusday() {
    if(this.add_quantity_number_thrusday <= 4)
    {
      console.log("this.add_quantity_number_thrusday ", this.add_quantity_number_thrusday);
  
      this.add_quantity_number_thrusday++;
      this.quantitiesthrusday().push(this.newQuantitythrusday());
    }
  }
  removeQuantitythrusday(i:number)
  {
    console.log("i ", i);
    if(i>0)
    {
      this.add_quantity_number_thrusday--;
      this.quantitiesthrusday().removeAt(i);
    }
    
  }
  newQuantitythrusday(): FormGroup {
    return this.fb.group({
      thrusdayStartTime: '',
      thrusdayEndTime: ''
    })
  }

  quantitiesfriday() : FormArray {
    return this.productForm.get("quantitiesfriday") as FormArray
  }
  addQuantityfriday() {
    if(this.add_quantity_number_friday <= 4)
    {
      console.log("this.add_quantity_number_friday ", this.add_quantity_number_friday);
  
      this.add_quantity_number_friday++;
      this.quantitiesfriday().push(this.newQuantityfriday());
    }
  }
  removeQuantityfriday(i:number)
  {
    console.log("i ", i);
    if(i>0)
    {
      this.add_quantity_number_friday--;
      this.quantitiesfriday().removeAt(i);
    }
    
  }
  newQuantityfriday(): FormGroup {
    return this.fb.group({
      fridayStartTime: '',
      fridayEndTime: ''
    })
  }

  quantitiessaturday() : FormArray {
    return this.productForm.get("quantitiessaturday") as FormArray
  }
  addQuantitysaturday() {
    if(this.add_quantity_number_saturday <= 4)
    {
      console.log("this.add_quantity_number_saturday ", this.add_quantity_number_saturday);
  
      this.add_quantity_number_saturday++;
      this.quantitiessaturday().push(this.newQuantitysaturday());
    }
  }
  removeQuantitysaturday(i:number)
  {
    console.log("i ", i);
    if(i>0)
    {
      this.add_quantity_number_saturday--;
      this.quantitiessaturday().removeAt(i);
    }
    
  }
  newQuantitysaturday(): FormGroup {
    return this.fb.group({
      saturdayStartTime: '',
      saturdayEndTime: ''
    })
  }


  quantitiessunday() : FormArray {
    return this.productForm.get("quantitiessunday") as FormArray
  }
  addQuantitysunday() {
    if(this.add_quantity_number_sunday <= 4)
    {
      console.log("this.add_quantity_number_sunday ", this.add_quantity_number_sunday);
  
      this.add_quantity_number_sunday++;
      this.quantitiessunday().push(this.newQuantitysunday());
    }
  }
  removeQuantitysunday(i:number)
  {
    console.log("i ", i);
    if(i>0)
    {
      this.add_quantity_number_sunday--;
      this.quantitiessunday().removeAt(i);
    }
    
  }
  newQuantitysunday(): FormGroup {
    return this.fb.group({
      sundayStartTime: '',
      sundayEndTime: ''
    })
  }

  onSubmit()
  {
    console.log("hereee");
    let cc = this.productForm.value;
    console.log("form value  ",cc);
    this.http.post(this.base_url+"api/seller/addTimeSloatSeller",{formdata:this.productForm.value}).subscribe((response)=>{
      console.log("response ", response);
      this.apiResponse = response;
      setTimeout(function (){
        window.location.reload();
      },2000);
    });
  }
}
