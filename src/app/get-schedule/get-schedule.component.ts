import { Component, OnInit } from '@angular/core';
import { SellerserviceService } from '../sellerservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-get-schedule',
  templateUrl: './get-schedule.component.html',
  styleUrls: ['./get-schedule.component.css']
})
export class GetScheduleComponent implements OnInit {
  baseurl: any
  ordersarray: any
  orders: any;
  myoptions: any
  main_id: any
  searchText: any; APIres: any
  categoryoptions:any
  catagory:any;condition:any; title:any; record:any;
  getallcatagoriesflat:any; getallcatagoriesflatRes:any; allCat:any
  
  constructor(private seller: SellerserviceService,
    private http: HttpClient, private activeRoute: ActivatedRoute, private router: Router)
    {
      this.baseurl = seller.baseapiurl2
      this.ordersarray = []
      this.orders = []
      this.main_id = localStorage.getItem('main_sellerid')
      
    }

  ngOnInit(): void {
    this.http.get(this.baseurl+"api/seller/getTimeSloatSeller/"+this.main_id).subscribe(res => {
      this.ordersarray = res;
      
      if(this.ordersarray.status == true)
      {
        this.record = this.ordersarray.record;
      }
      
      //this.orders = this.ordersarray.data
      console.log("order ", res);
    })
  }

  deleteSchedule(event:any,val_id:any)
  {
    console.log("val_id " , val_id);
    if(confirm("Êtes-vous sûr de vouloir supprimer cet enregistrement ?"))
    {
      console.log("ifffffffffffffffff " , val_id);
      this.http.get(this.baseurl+"api/seller/deleteTimeSloatSeller/"+val_id ).subscribe(res => {
        this.ordersarray = res;
        if(this.ordersarray.status == true)
        {
          window.location.reload();
        }
        
        //this.orders = this.ordersarray.data
        console.log("order ", res);
      })
    }
  }

}
